
public interface InsurancePremium {

	public double premiumCalucationBasedOnAge();
	public double premiumCalucationBasedOnGender();
	public double premiumCalucationBasedOnHealth();
	public double premiumCalucationBasedOnHabits();

}
